from mockup_generator import Mockup3DGenerator

ARTWORK_DATA = {
    "Back": "data/test-artwork.jpg",
    "Front": "data/test-artwork.jpg",
    "Left Sleeve": "data/test-artwork.jpg",
    "Right Sleeve": "data/test-artwork.jpg",
}

MOCKUP_DATA1 = [
    {
        "side_name": "Tshirt",
        "parts": [
            {
                "name": "",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Change your BG Here.png",
            },
            {
                "name": "",
                "cut_image_path": "./sample_data/mask_part/Tshirt/T-shirt 3D.png",
            },
            {
                "name": "Design.Front",
                "model_path": "./sample_data/models/Tshirt/Front.model.npy",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Design.Front.png",
                "shadow_image": "",
                "artwork_side": "Front"
            },
            {
                "name": "Design.Left Sleeve",
                "model_path": "./sample_data/models/Tshirt/Left Sleeve.model.npy",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Design.Left Sleeve.png",
                "shadow_image": "",
                "artwork_side": "Left Sleeve"
            },
            {
                "name": "Design.Right Sleeve",
                "model_path": "./sample_data/models/Tshirt/Right Sleeve.model.npy",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Design.Right Sleeve.png",
                "shadow_image": "",
                "artwork_side": "Right Sleeve"
            },
            {
                "name": "Design.Neck Back",
                "model_path": "./sample_data/models/Tshirt/Neck Back.model.npy",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Design.Neck Back.png",
                "shadow_image": "",
                "artwork_side": "Back"
            },
            {
                "name": "Design.Neck Front",
                "model_path": "./sample_data/models/Tshirt/Neck Front.model.npy",
                "cut_image_path": "./sample_data/mask_part/Tshirt/Design.Neck Front.png",
                "shadow_image": "",
                "artwork_side": "Front"
            },
            {
                "name": "",
                "cut_image_path": "./sample_data/mask_part/Tshirt/fx.fx2.png",
            },
            {
                "name": "",
                "cut_image_path": "./sample_data/mask_part/Tshirt/fx.fx1.png",
            }
        ]
    }
]

MOCKUP_DATA = [
    {
        "side_name": "Tshirt",
        "parts": [
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Change your BG Here.png"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.T-shirt 3D.png"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Design.Front.png",
                "model_path": "data/T-Shirt/model/T-Shirt Front.Design.Front.model.npy",
                "artwork_side": "Front"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Design.Left Sleeve.png",
                "model_path": "data/T-Shirt/model/T-Shirt Front.Design.Left Sleeve.model.npy",
                "artwork_side": "Left Sleeve"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Design.Right Sleeve.png",
                "model_path": "data/T-Shirt/model/T-Shirt Front.Design.Right Sleeve.model.npy",
                "artwork_side": "Right Sleeve"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Design.Neck Back.png",
                "model_path": "data/T-Shirt/model/T-Shirt Front.Design.Neck Back.model.npy",
                "artwork_side": "Back"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.Design.Neck Front.png",
                "model_path": "data/T-Shirt/model/T-Shirt Front.Design.Neck Front.model.npy",
                "artwork_side": "Front"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.fx.fx2.png"
            },
            {
                "name": "",
                "cut_image_path": "data/T-Shirt/img_mask/T-Shirt Front.fx.fx1.png"
            }
        ]
    }
]

if __name__ == "__main__":
    mockup = Mockup3DGenerator(mockup_data=MOCKUP_DATA, artwork_data=ARTWORK_DATA, background_data={})
    print("----- STARTING -----")
    mockup.do_generate()
    print("----- DONE -----")
