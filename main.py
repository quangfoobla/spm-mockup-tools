from actions.make_artworks import make_artworks
from actions.make_mask import make_masks
from actions.make_img_mapping import make_img_mapping
from actions.make_models import make_models
from actions.generate_config import generate_config
from actions.preview_model import preview_model
from actions.make_sub_artworks import make_sub_artworks

product_name = '3d-jersey-tank'
row = 25
col = 25
point_stroke = 5


def main():
    # make_artworks(product_name, col, row, point_stroke)
    # make_masks(product_name)
    # make_img_mapping(product_name)
    # make_models(product_name, row, col)
    # make_sub_artworks(product_name, 'Front', f'data/{product_name}/Front.png')
    # generate_config(product_name)
    preview_model(product_name)


if __name__ == '__main__':
    main()
