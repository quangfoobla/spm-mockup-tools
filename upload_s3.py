import logging
import boto3
from botocore.exceptions import ClientError
import glob
import os
import json
from PIL import Image
import math
import requests
import time

ACCESS_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZWY5YTk2NWUzYzY1NWQ1Yzc0NWQ0ZjUiLCJlbWFpbCI6InF1YW5nZHZAZm9vYmxhLmNvbSIsImlhdCI6MTYyMTI0ODI1NSwiZXhwIjoxNjIzODQwMjU1fQ.26Pvk-dYN58MnFm33NEF3W-Xff3RQsPxKz7q0wWyPtw'

ACCESS_KEY = 'MTCQ6GS2FVJDROA6ISDV'
SECRET_KEY = 'uC0bypM7AIw5OvynqIp+/6Gk6ZWZ2Lzg9ws0xZHDS8k'
ENDPOINT_URL = 'https://nyc3.digitaloceanspaces.com'
BUCKET = 'platform126-dev'

session = boto3.session.Session()
s3_client = session.client(
    's3',
    region_name='nyc3',
    endpoint_url=ENDPOINT_URL,
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY
)


def upload_file(file_name, bucket, object_name=None):
    if object_name is None:
        object_name = file_name

    try:
        s3_client.upload_file(file_name, bucket, object_name, ExtraArgs={'ACL': 'public-read'})
        return f'{ENDPOINT_URL}/{bucket}/{object_name}'
    except ClientError as e:
        logging.error(e)
        return False

    return True


def create_safezone(product_name, dpi):
    safezone_dir = f'data/{product_name}/safezone'

    preview = {}
    side_infos = []

    for file in glob.glob(f'{safezone_dir}/*.png'):
        safezone = Image.open(file)
        width, height = safezone.size
        file_name = os.path.basename(file)
        object_name = f'product-mockups/{product_name}/safezone/{file_name}'
        safezone_url = upload_file(file, BUCKET, object_name)
        side = file_name.replace('.png', '')

        preview[side] = {
            "frame_x": math.floor(width / 2),
            "frame_y": math.floor(height / 2),
            "image_path": safezone_url,
            "frame_width": width,
            "image_width": width,
            "frame_height": height,
            "image_height": height
        }

        side_infos.append({
            "id": "12079089670698",
            "type": side,
            "create_time": "2021-03-30T02:56:20.982Z",
            "update_time": "2021-03-30T02:56:20.982Z",
            "abstract_product_id": "12079087006925",
            "constraints": {
                "minimum_dpi": dpi
            },
            "fusion_size": {
                "artwork_fusion_size": {
                    "width": width,
                    "height": height,
                    "width_in_inch": width / dpi,
                    "height_in_inch": height / dpi
                }
            },
            "enable_background_color": "t",
            "fulfill_require_artwork": "t"
        })

    return {
        "preview": preview,
        "side_infos": side_infos
    }


def create_mockup_infos(product_name):
    config_dir = f'data/{product_name}/config'

    mockup_infos = []

    for file in glob.glob(f'{config_dir}/*.mockup-data.json'):
        mockup_f = open(file)
        mockup_data = json.load(mockup_f)

        parts = []

        for part in mockup_data["parts"]:
            item = {
                "name": part["name"]
            }

            if 'image_path' in part:
                image_name = os.path.basename(part["image_path"])
                image_aws_path = f'product-mockups/{product_name}/image-mask/{image_name}'
                print('Uploading:', image_aws_path)
                item["image_path"] = upload_file(part["image_path"], BUCKET, image_aws_path)

            if 'model' in part:
                model_name = os.path.basename(part["model"])
                model_aws_path = f'product-mockups/{product_name}/model/{model_name}'
                print('Uploading:', model_aws_path)
                item["model"] = upload_file(part["model"], BUCKET, model_aws_path)

            if 'side' in part:
                item["side"] = part["side"].split('.')[1].lower()

            parts.append(item)

        mockup_infos.append({
            "name": mockup_data['side_name'],
            "side": mockup_data["side_name"].lower(),
            "type": "warp-standard",
            "parts": parts,
            "width": 1000,
            "height": 1000
        })

    return mockup_infos


def create_product_mockup(product_name, dpi):
    product_dir_name = product_name.lower().replace(' ', '-')
    prefix_name = product_name.lower().replace(' ', '_')
    safezone_data = create_safezone(product_dir_name, dpi)
    mockup_infos = create_mockup_infos(product_dir_name)

    product_mockup = {
        "preview": safezone_data["preview"],
        "meta": {
            "mockup_infos": mockup_infos,
            "consistency_name": prefix_name,
            "artwork_side_info": "artwork-per-side",
            "prefix_name": prefix_name,
            "version": "v2"
        },
        "preview_meta": {
            "z_index": 1,
            "enable_preview": 1,
            "color_attr_interactive": 0,
            "default_material_color": "#ffffff"
        },
        "side_infos": safezone_data["side_infos"],
        "title": product_name,
        "product_type": prefix_name,
        "size_chart": [],
        "wrap_type": "curve"
    }

    url = 'https://seller-qa.merch8.com/api/product-line/manager/mockup'
    headers = {
        "Authorization": f'Bearer {ACCESS_TOKEN}'
    }
    requests.post(url, json=product_mockup, headers=headers)


def create_product_line(product_name):
    prefix_name = product_name.lower().replace(' ', '_')

    product_line = {
        "title": product_name,
        "product_type": prefix_name,
        "attributes": [
            {
                "name": "Color",
                "value_type": "Color",
                "values": [
                    {
                        "name": "Black",
                        "value": "#000000"
                    },
                    {
                        "name": "White",
                        "value": "#FFFFFF"
                    }
                ],
                "hide_attribute_storefront": False,
                "is_active": True,
                "is_preselected": True
            },
            {
                "name": "Size",
                "value_type": "Size",
                "values": [
                    {
                        "name": "S",
                        "value": "S"
                    },
                    {
                        "name": "M",
                        "value": "M"
                    },
                    {
                        "name": "L",
                        "value": "L"
                    },
                    {
                        "name": "XL",
                        "value": "XL"
                    },
                    {
                        "name": "2XL",
                        "value": "2XL"
                    },
                    {
                        "name": "3XL",
                        "value": "3XL"
                    },
                    {
                        "name": "4XL",
                        "value": "4XL"
                    },
                    {
                        "name": "5XL",
                        "value": "5XL"
                    }
                ],
                "hide_attribute_storefront": False,
                "is_active": True,
                "is_preselected": False
            }
        ],
        "available_for_store_modes": [
            "normal"
        ],
        "affiliate_category": "",
        "is_popular": False,
        "currency": "USD",
        "available_for_campaign": True,
        "est_process_time_to": 6,
        "est_process_time_from": 2,
        "sku_prefix": "SPM_B753",
        "description": "<p>SPM Mockup Test Description</p>\n",
        "category": "60364175882786e5a4005c0b"
    }

    url = 'https://seller-qa.merch8.com/api/product-line/product-lines'
    headers = {
        "Authorization": f'Bearer {ACCESS_TOKEN}'
    }
    requests.post(url, json=product_line, headers=headers)


def setup_mockup(product_name, dpi=300):
    create_product_mockup(product_name, dpi)
    time.sleep(5)
    create_product_line(product_name)


def main():
    setup_mockup('3D Cap BBCP')


if __name__ == '__main__':
    main()
