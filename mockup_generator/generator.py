import os
import time

import cv2
import numpy as np
from PIL import Image, ImageChops

import thinplate as tps
import actions.constants as constants


class Mockup3DGenerator:
    def __init__(self, mockup_data, artwork_data, background_data, colors="#333333"):
        self.mockup_data = mockup_data
        self.artwork_data = artwork_data
        self.background_data = background_data
        self.colors = colors

    def do_generate(self, mockup_output):
        for side in self.mockup_data:
            start = time.time()
            main_side_image = Image.new("RGBA", (1000, 1000), (255, 255, 255))
            for part in side['parts']:
                if "model" not in part:
                    image = Image.open(part['image_path']).convert("RGBA")
                    main_side_image = Image.alpha_composite(main_side_image, image).convert("RGBA")
                else:
                    print('{} has  model'.format(part.get("name")))
                    artwork_image = Image.open(self.artwork_data[part['side']]).convert("RGBA")
                    artwork_image = np.array(artwork_image)
                    open_cv_artwork_image = cv2.cvtColor(artwork_image, cv2.COLOR_RGB2BGR)
                    resized_img = cv2.resize(open_cv_artwork_image, constants.shape)
                    cv2.imwrite('resized_img.png', resized_img)

                    if os.path.isfile(part['model']):

                        grid = np.load(part['model'], allow_pickle=True)

                        mapx, mapy = tps.tps_grid_to_remap(grid, constants.shape)
                        img = cv2.remap(resized_img, mapx, mapy, cv2.INTER_CUBIC)
                        img = cv2.cvtColor(img, cv2.COLOR_BGRA2RGBA)
                        cv2.imwrite('img.png', img)
                        warped = Image.fromarray(img).convert("RGBA")
                        cut_image = Image.open(part['image_path']).convert("RGBA")
                        main_side_image = Image.composite(warped, main_side_image, cut_image)
                    else:
                        print("Model for {} not found".format(side['side_name'] + " | " + part['name']))
            main_side_image = main_side_image.resize((3000, 3000), Image.ANTIALIAS)
            end = time.time()
            print('[{:.4f} s] Generate mockup for {} executed'.format((end - start), side['side_name']))
            main_side_image.resize(constants.shape)
            main_side_image.save(mockup_output)
            # main_side_image.show()
