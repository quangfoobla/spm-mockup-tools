from actions.warp_image import warp_image


def main():
    model = 'data/engraved-leather-wristlet-keychain/model/Scene 1.Design.Front.model.npy'
    artwork = 'data/engraved-leather-wristlet-keychain/artworks/Scene 1.Front.png'
    output = 'data/engraved-leather-wristlet-keychain/test-warp.png'
    warp_image(model, artwork, output)


if __name__ == '__main__':
    main()
