hex_list = ['00', '40', '80', 'c0', 'ff']

colors = []

for hex1 in hex_list:
    for hex2 in hex_list:
        for hex3 in hex_list:
            hex_color = '#' + hex1 + hex2 + hex3
            colors.append(hex_color)

print(len(colors))

from PIL import Image, ImageDraw

w, h = 2000, 2000
img = Image.new("RGBA", (w, h))
img1 = ImageDraw.Draw(img)

for x in range(0, int(w / 40)):
    shape = [(x * 40, 0), (x * 40, h)]
    img1.line(shape, fill=colors[x], width=5)

for y in range(0, int(h / 40)):
    shape = [(0, y * 40), (w, y * 40)]
    img1.line(shape, fill=colors[y + 50], width=5)

img.save('test.png')
