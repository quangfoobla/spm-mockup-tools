import os
import random
import string


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def random_string(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
