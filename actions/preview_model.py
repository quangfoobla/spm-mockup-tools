import glob
import os
import json
from actions.warp_image import warp_image
from mockup_generator import Mockup3DGenerator
from actions.utils import ensure_dir


def make_preview_image(model_path, artwork_path, output_file):
    warp_image(model_path, artwork_path, output_file)


def preview(product_name, psd_name):
    artwork_config_path = f'data/{product_name}/config/{psd_name}.artworks.json'
    artwork_f = open(artwork_config_path)
    artworks = json.load(artwork_f)
    mockup_config_path = f'data/{product_name}/config/{psd_name}.mockup-data.json'
    mockup_f = open(mockup_config_path)
    mockup_data = json.load(mockup_f)
    parts = mockup_data['parts']

    for part in parts:
        if 'model' in part.keys():
            model_path = part['model']
            artwork_path = artworks[part['side']]
            model_name = os.path.basename(model_path).replace('.model.npy', '')
            output_file = f'data/{product_name}/img_warped/{model_name}.png'
            make_preview_image(model_path, artwork_path, output_file)

    mockup = Mockup3DGenerator(mockup_data=[mockup_data], artwork_data=artworks, background_data={})
    print('Start generate mockup', psd_name)
    mockup_output = f'data/{product_name}/mockup/{psd_name}.png'
    ensure_dir(f'data/{product_name}/mockup')
    mockup.do_generate(mockup_output)


def preview_model(product_name):
    psd_template_dir = f'data/{product_name}/psd-artwork'

    for file in glob.glob(f'{psd_template_dir}/*.psd'):
        psd_name = os.path.basename(file).replace('.psd', '')
        preview(product_name, psd_name)
