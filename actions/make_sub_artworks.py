import glob
import os
import json
from PIL import Image, ImageCms
from actions.utils import ensure_dir


def convert_to_rgb(image):
    if image.mode == 'CMYK':
        return ImageCms.profileToProfile(image, 'profiles/USWebCoatedSWOP.icc', 'profiles/sRGB.icm', renderingIntent=0,
                                         outputMode='RGB')

    return image.convert('RGB')


def generate_artworks(product_name, side, artwork_side, artwork_path):
    config_path = f'data/{product_name}/artworks/{side}.artwork-positions.json'
    test_artworks_dir = f'data/{product_name}/test-artworks'
    ensure_dir(test_artworks_dir)
    artwork = Image.open(artwork_path)
    rgb_artwork = convert_to_rgb(artwork)

    print(product_name, side, artwork_side)

    with open(config_path) as f:
        data = json.load(f)

        for key in data:
            if key.startswith(f'{side}.{artwork_side}'):
                print(key)
                print(data[key])
                left = data[key]["left"]
                top = data[key]["top"]
                right = left + data[key]["width"]
                bottom = top + data[key]["height"]
                output_path = f'{test_artworks_dir}/{key}.png'
                rgb_artwork.crop((left, top, right, bottom)).save(output_path)


def make_sub_artworks(product_name, artwork_side, artwork_path):
    psd_dir = f'data/{product_name}/psd-artwork'

    print(psd_dir)

    for file in glob.glob(f'{psd_dir}/*.psd'):
        side = os.path.basename(file).replace('.psd', '')
        generate_artworks(product_name, side, artwork_side, artwork_path)
