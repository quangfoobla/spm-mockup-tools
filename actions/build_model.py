import json
import numpy as np
import thinplate as tps
import actions.constants as constants
import os
from actions.utils import ensure_dir


def build(_output, _c_src, _c_dst):
    theta = tps.tps_theta_from_points(_c_src, _c_dst, reduced=True)
    grid = tps.tps_grid(theta, _c_dst, constants.shape)

    model_name = _output.replace('.npy', '')
    ensure_dir(os.path.dirname(_output))

    np.save(model_name, grid)

    return _output


def build_model(trans_path, output):
    try:
        print(f'Start build model form {trans_path}')

        with open(trans_path) as f:
            data = json.load(f)

            name = build(
                _output=output,
                _c_src=np.array(data["src"]),
                _c_dst=np.array(data["dst"]),
            )

            print(f'Built model {name}')

            return output
    except Exception as e:
        print(e)
