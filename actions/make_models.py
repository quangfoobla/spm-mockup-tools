import glob
import os
from actions.make_mapping import make_mapping
from actions.build_model import build_model


def make_transformation(product_name, row, col, img_path):
    name = os.path.basename(img_path).replace('.png', '')
    transformation_output = f'data/{product_name}/transformation/{name}.json'
    make_mapping(transformation_output, img_path, row, col)


def make_model(product_name, trans_path):
    name = os.path.basename(trans_path).replace('.json', '')
    model_output = f'data/{product_name}/model/{name}.model.npy'
    build_model(trans_path, model_output)


def make_models(product_name, row, col):
    img_mapping_dir = f'data/{product_name}/img_mapping'

    for file in glob.glob(f'{img_mapping_dir}/*.png'):
        make_transformation(product_name, row, col, file)

    trans_dir = f'data/{product_name}/transformation'

    for file in glob.glob(f'{trans_dir}/*.json'):
        make_model(product_name, file)
