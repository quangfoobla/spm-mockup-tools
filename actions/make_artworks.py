import glob
import os
import json
from PIL import Image
from psd_tools import PSDImage
from actions.utils import random_string, ensure_dir
from actions.make_grid import make_grid


def make_psd_artworks(psd_path, product_name, col, row, point_stroke):
    print(f'Make artwork from {psd_path}')
    psd = PSDImage.open(psd_path)
    psd_name = os.path.basename(psd_path).replace('.psd', '')

    bg_blue_color = 255

    for layer in psd:
        if layer.name == 'Artworks':
            artwork_colors = {}
            artwork_positions = {}

            for item in layer:
                temp_artwork_path = f'temp/psd-base/{random_string(10)}.psd'
                ensure_dir('temp/psd-base')
                item.smart_object.save(temp_artwork_path)
                artwork_psd = PSDImage.open(temp_artwork_path)
                os.unlink(temp_artwork_path)

                img = Image.new('RGBA', (artwork_psd.width, artwork_psd.height), (0, 0, 0, 0))

                for artwork_item in artwork_psd:
                    if artwork_item.name == 'Artworks':
                        for artwork_side in artwork_item:
                            print(artwork_side.name, artwork_side.top, artwork_side.left, artwork_side.width,
                                  artwork_side.height)

                            img_grid = make_grid(artwork_side.width, artwork_side.height, col, row, point_stroke,
                                                 (255, 255, bg_blue_color))
                            artwork_colors[f'{psd_name}.{item.name}.{artwork_side.name}'] = (255, 255, bg_blue_color)
                            artwork_positions[f'{psd_name}.{item.name}.{artwork_side.name}'] = {
                                "top": artwork_side.top,
                                "left": artwork_side.left,
                                "width": artwork_side.width,
                                "height": artwork_side.height,
                            }
                            img.paste(img_grid, (artwork_side.left, artwork_side.top))
                            img_grid_dir = f'data/{product_name}/img_grid'
                            ensure_dir(img_grid_dir)
                            img_grid.save(f'{img_grid_dir}/{psd_name}.{item.name}.{artwork_side.name}.png')

                            bg_blue_color -= 1

                output_dir = f'data/{product_name}/artworks'
                ensure_dir(output_dir)
                output_png_path = f'{output_dir}/{psd_name}.{item.name}.png'
                img.save(output_png_path)

            output_file = f'data/{product_name}/artworks/{psd_name}.artworks.json'
            with open(output_file, 'w') as fp:
                json.dump(artwork_colors, fp)

            position_output_file = f'data/{product_name}/artworks/{psd_name}.artwork-positions.json'
            with open(position_output_file, 'w') as fp:
                json.dump(artwork_positions, fp)


def make_artworks(product_name, col, row, point_stroke):
    psd_template_dir = f'data/{product_name}/psd-base'

    print(f'Make artworks from dir {psd_template_dir}')

    for file in glob.glob(f'{psd_template_dir}/*.psd'):
        make_psd_artworks(file, product_name, col, row, point_stroke)
