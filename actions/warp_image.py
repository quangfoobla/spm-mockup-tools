import cv2
import numpy as np
import actions.constants as constants
import thinplate as tps
import os
from actions.utils import ensure_dir


def warp_image_cv(_img, _model):
    grid = np.load(_model)
    mapx, mapy = tps.tps_grid_to_remap(grid, constants.shape)
    _out = cv2.remap(_img, mapx, mapy, cv2.INTER_AREA)
    print(_out.shape)
    return _out


def do_warp(_model, _infile, _outfile):
    img = cv2.imread(_infile)
    resized_img = cv2.resize(img, constants.shape)

    warped = warp_image_cv(resized_img, _model)

    ensure_dir(os.path.dirname(_outfile))

    cv2.imwrite(_outfile, warped)

    return _outfile


def warp_image(model_path, artwork_path, output_file):
    try:
        do_warp(model_path, artwork_path, output_file)
    except Exception as e:
        print(e)
