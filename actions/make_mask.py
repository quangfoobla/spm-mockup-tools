import glob
import json
import os
from PIL import Image
from psd_tools import PSDImage
from actions.utils import ensure_dir
import actions.constants as constants


def export_layer_mask(psd, layer, prefix, product_name):
    output_dir = f'data/{product_name}/img_mask'
    img_output = f'{output_dir}/{prefix}{layer.name}.png'
    ensure_dir(output_dir)

    img = Image.new('RGBA', (psd.width, psd.height), (0, 0, 0, 0))
    img.paste(layer.composite(), (layer.left, layer.top))
    img.resize(constants.shape).save(img_output)

    return img_output


def make_composite_masks(psd_path, product_name):
    print(f'Make composite masks from {psd_path}')

    psd = PSDImage.open(psd_path)

    psd_name = os.path.basename(psd_path).replace('.psd', '')

    masks = []

    for layer in psd:
        if layer.is_visible():
            if layer.is_group():
                for item in layer:
                    if item.is_group():
                        for item2 in item:
                            if not item2.is_group():
                                mask_path = export_layer_mask(psd, item2, f'{psd_name}.{layer.name}.{item.name}.',
                                                              product_name)
                                masks.append(mask_path)
                            else:
                                for item3 in item2:
                                    mask_path = export_layer_mask(psd, item3
                                                                  , f'{psd_name}.{layer.name}.{item.name}.{item2.name}.'
                                                                  , product_name)
                                    masks.append(mask_path)
                    else:
                        mask_path = export_layer_mask(psd, item, f'{psd_name}.{layer.name}.', product_name)
                        masks.append(mask_path)
            else:
                mask_path = export_layer_mask(psd, layer, f'{psd_name}.', product_name)
                masks.append(mask_path)

    output_dir = f'data/{product_name}/img_mask'
    ensure_dir(output_dir)
    output_file = f'{output_dir}/{psd_name}.mask.json'

    with open(output_file, 'w') as fp:
        json.dump(masks, fp)


def make_masks(product_name):
    psd_mask_dir = f'data/{product_name}/psd-base'

    for file in glob.glob(f'{psd_mask_dir}/*.psd'):
        make_composite_masks(file, product_name)
