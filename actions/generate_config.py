import glob
import os
import json
import extcolors
from actions.utils import ensure_dir
from PIL import Image


def get_model_path(product_name, mask_path):
    mask_name = os.path.basename(mask_path).replace('.png', '')
    model_path = f'data/{product_name}/model/{mask_name}.model.npy'

    if os.path.isfile(model_path):
        return model_path
    else:
        return ''


def get_artwork_side(product_name, psd_name, mask_path):
    artwork_config_path = f'data/{product_name}/artworks/{psd_name}.artworks.json'

    mask_name = os.path.basename(mask_path).replace('.png', '')
    img_mapping_path = f'data/{product_name}/img_mapping/{mask_name}.png'

    if os.path.isfile(img_mapping_path):
        colors, pixel_count = extcolors.extract_from_path(img_mapping_path)

        f = open(artwork_config_path)
        data = json.load(f)
        f.close()

        for key, value in data.items():
            if list(colors[0][0]) == value:
                return key

        return ''
    else:
        return ''


def calculate_rotate(item_path):
    im = Image.open(item_path)
    (left, top, right, bottom) = im.getbbox()
    warp_info = {"top": top, "left": left, "width": right - left, "height": bottom - top}
    area = (right - left) * bottom - top
    rotate = 0

    for i in range(90):
        box = im.rotate(i, expand=True).getbbox()
        width = box[2] - box[0]
        height = box[3] - box[1]
        new_area = width * height
        if new_area < area:
            area = new_area
            warp_info = {"top": box[1], "left": box[0], "width": width, "height": height}
            rotate = i

    return {
        "rotate": rotate,
        "warp_info": warp_info
    }


def generate(product_name, psd_name):
    mask_config_path = f'data/{product_name}/img_mask/{psd_name}.mask.json'

    f = open(mask_config_path)
    data = json.load(f)

    parts = []

    for item in data:
        part = {
            'name': os.path.basename(item).replace('.png', ''),
            'image_path': item
        }

        model_path = get_model_path(product_name, item)
        artwork_side = get_artwork_side(product_name, psd_name, item)

        if model_path != '':
            part['model'] = model_path

        if artwork_side != '':
            part['side'] = artwork_side

        # if '.Design.' in item:
        #     del part['image_path']
        #     (left, top, right, bottom) = Image.open(item).getbbox()
            # abc = calculate_rotate(item)
            # part['warp_type'] = 'warp_2d'
            # part['warp_info'] = {"top": top, "left": left, "width": right - left, "height": bottom - top}
            # part['warp_info']['rotate'] = abc['rotate']
            # part['fill'] = 0
            # part['effects'] = [
            #     {"type": "color_overlay", "color": "#000000", "opacity": 0.56},
            #     {"type": "inner_glow", "color": "#000000", "size": 3, "opacity": 0.6},
            #     {"type": "drop_shadow", "angle": 177, "distance": 14, "size": 14, "color": "#ffffff", "opacity": 0.04}
            # ]
            # part['side'] = 'front'

        parts.append(part)

    f.close()

    output_dir = f'data/{product_name}/config'
    ensure_dir(output_dir)
    output_file = f'{output_dir}/{psd_name}.mockup-data.json'

    mockup_data = {
        'side_name': psd_name,
        'parts': parts
    }

    with open(output_file, 'w') as fp:
        json.dump(mockup_data, fp)

    artworks = {}

    img_grid_dir = f'data/{product_name}/img_grid'
    for file in glob.glob(f'{img_grid_dir}/{psd_name}*.png'):
        artwork_name = os.path.basename(file).replace('.png', '')
        artworks[artwork_name] = file

    output_file_artworks = f'{output_dir}/{psd_name}.artworks.json'
    with open(output_file_artworks, 'w') as fp:
        json.dump(artworks, fp)


def generate_config(product_name):
    print('Generate config')

    psd_template_dir = f'data/{product_name}/psd-artwork'

    for file in glob.glob(f'{psd_template_dir}/*.psd'):
        psd_name = os.path.basename(file).replace('.psd', '')
        generate(product_name, psd_name)
