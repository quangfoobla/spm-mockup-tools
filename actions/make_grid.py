from PIL import Image


def fill_point(pixels, width, height, x, y, color, expand_radius):
    for i in range(-expand_radius, expand_radius + 1):
        if (x + i >= 1) & (x + i <= width):
            for j in range(-expand_radius, expand_radius + 1):
                if (y + j >= 1) & (y + j <= height):
                    pixels[x + i - 1, y + j - 1] = color


def make_grid(width, height, grid_col, grid_row, expand_radius, bg_color):
    image = Image.new("RGBA", (width, height), color=bg_color)
    pixels = image.load()
    col_spacing = width / grid_col
    row_spacing = height / grid_row

    for i in range(0, grid_col + 1):
        for j in range(0, grid_row + 1):
            x = col_spacing * i
            y = row_spacing * j
            color = (i, j, 1)
            fill_point(pixels, width, height, x, y, color, expand_radius)

    return image
