import json
import os
from collections import defaultdict
from statistics import median
from actions.utils import ensure_dir
from PIL import Image, ImageDraw


def find_all_pixels_with_color(pixels, width, height, color):
    results = []
    for i in range(0, width):
        for j in range(0, height):
            pixel = pixels[i, j]
            if pixel[0] == color[0] and pixel[1] == color[1] and pixel[2] == color[2]:
                results.append([i, j])
    return results


def filter_pixel(pixels, is_left_edge, is_right_edge, is_top_edge, is_bot_edge):
    hor_filtered_pixels = pixels
    if is_left_edge:
        filtered_pixels = []
        all_y = set([pixel[1] for pixel in pixels])
        for y in all_y:
            most_left = min(pixel[0] for pixel in pixels if pixel[1] == y)
            filtered_pixels.append([most_left, y])
        hor_filtered_pixels = filtered_pixels
    elif is_right_edge:
        filtered_pixels = []
        all_y = set([pixel[1] for pixel in pixels])
        for y in all_y:
            most_right = max(pixel[0] for pixel in pixels if pixel[1] == y)
            filtered_pixels.append([most_right, y])
        hor_filtered_pixels = filtered_pixels
    ver_filtered_pixels = pixels
    if is_top_edge:
        filtered_pixels = []
        all_x = set([pixel[0] for pixel in pixels])
        for x in all_x:
            most_top = min(pixel[1] for pixel in pixels if pixel[0] == x)
            filtered_pixels.append([x, most_top])
        ver_filtered_pixels = filtered_pixels
    elif is_bot_edge:
        filtered_pixels = []
        all_x = set([pixel[0] for pixel in pixels])
        for x in all_x:
            most_bot = max(pixel[1] for pixel in pixels if pixel[0] == x)
            filtered_pixels.append([x, most_bot])
        ver_filtered_pixels = filtered_pixels
    both_dimension_filtered_pixels = []
    for hor_pixel in hor_filtered_pixels:
        for ver_pixel in ver_filtered_pixels:
            if ver_pixel[0] == hor_pixel[0] and ver_pixel[1] == hor_pixel[1]:
                both_dimension_filtered_pixels.append(hor_pixel)
                break
    return both_dimension_filtered_pixels


def is_border(i, j, pixels, width, height):
    thickness = 2
    for x in range(i - thickness, i + thickness):
        for y in range(j - thickness, j + thickness):
            if 0 <= x < width and 0 <= y < height:
                pixel = pixels[x, y]
                if pixel[2] != 1:
                    return True
    return False


def make_mapping(output_file, warped_image, grid_row, grid_col):
    image = Image.open(warped_image).convert('RGB')
    pixels = image.load()
    width = image.size[0]
    height = image.size[1]
    src = []
    dst = []
    row_spacing = 1.0 / float(grid_col)
    col_spacing = 1.0 / float(grid_row)
    grid_dict = defaultdict(list)

    # name = os.path.basename(output_file).replace('.json', '')

    for i in range(0, width):
        for j in range(0, height):
            pixel = pixels[i, j]
            if pixel[2] == 1:
                is_pixel_border = is_border(i, j, pixels, width, height)
                if not is_pixel_border:
                    grid_dict[(pixel[0], pixel[1])].append([i, j])

    # img111 = Image.new('RGBA', (5000, 5000), (255, 255, 255))
    # draw = ImageDraw.Draw(img111)

    for i in range(0, grid_col + 1):
        for j in range(0, grid_row + 1):
            matched_pixels = grid_dict[(i, j)]
            if len(matched_pixels) > 0:
                matched_pixels = filter_pixel(pixels=matched_pixels,
                                              is_left_edge=(i == 0), is_right_edge=(i == grid_col),
                                              is_top_edge=(j == 0), is_bot_edge=(j == grid_row))
                center_pixel_x = median([pixel[0] for pixel in matched_pixels])
                center_pixel_y = median([pixel[1] for pixel in matched_pixels])
                src.append([i * row_spacing, j * col_spacing])
                dst.append([center_pixel_x / width, center_pixel_y / height])

                # draw.rectangle([(center_pixel_x - 5, center_pixel_y - 5), (center_pixel_x + 5, center_pixel_y +
                # 5)], (1, i, j))

    # print(output_file)
    # print(name)
    # img111.save(f'{name}.png')

    data = {
        "src": src,
        "dst": dst
    }

    ensure_dir(os.path.dirname(output_file))

    with open(output_file, 'w') as fp:
        json.dump(data, fp)

    return output_file
