import cv2
import numpy as np
from PIL import Image


def thresholding(image, background_color):
    mask = image.copy().convert('RGBA')
    im1 = Image.new('RGBA', mask.size, '#FFF')
    im2 = Image.new('RGBA', mask.size, background_color)
    im = Image.composite(im1, im2, mask)

    return im


def expand_image(image, size):
    width, height = image.size
    result = Image.new('RGBA', (width + size, height + size))
    result.paste(image, (int(size / 2), int(size / 2)))

    return result


def get_contours(image):
    opencv_image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)

    LUV = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2LUV)
    edges = cv2.Canny(LUV, 10, 100)
    contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    return contours


def count_contours(image):
    contours = get_contours(image)

    return len(contours)


def export_contour(image, contour):
    width, height = image.size
    blank_image = np.zeros((height, width, 3), np.uint8)
    cv2.drawContours(blank_image, [contour], -1, (255, 255, 255), -1)

    color_converted = cv2.cvtColor(blank_image, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(color_converted)

    return pil_image


def make_part_image(expand_wrap, contour_image):
    mask = contour_image.convert('L')
    background = Image.new('RGBA', expand_wrap.size)
    im = Image.composite(expand_wrap, background, mask)
    width, height = expand_wrap.size

    return im.crop((50, 50, width - 50, height - 50))


def trim_part(wrap, mask):
    expand_wrap = expand_image(wrap, 100)
    expand_mask = expand_image(mask, 100)
    threshold_wrap = thresholding(expand_wrap, (0, 0, 0))
    threshold_mask = thresholding(expand_mask, (0, 0, 0, 0))

    contours = get_contours(threshold_wrap)

    i = 0
    for contour in contours:
        contour_image = export_contour(threshold_wrap, contour)
        im2 = Image.new('RGBA', mask.size, '#000')
        im = Image.composite(contour_image, im2, threshold_mask)
        num_of_contours = count_contours(im)

        if num_of_contours >= 1:
            result = make_part_image(expand_wrap, contour_image)

            return result

        i += 1

    return wrap
