import glob
import os
from PIL import Image
from psd_tools import PSDImage
from actions.utils import ensure_dir
from actions.trim_img_mapping import trim_part


def export_psd_groups(product_name, psd, layer, prefix):
    for item in layer:
        if item.is_visible():
            output_dir = f'data/{product_name}/img_mapping'
            img_output = f'{output_dir}/{prefix}{item.name}.png'
            ensure_dir(output_dir)

            print(img_output)
            print(psd.width, psd.height, item.left, item.top, item.width, item.height)

            img = Image.new('RGBA', (psd.width, psd.height), (0, 0, 0, 0))
            img.paste(item.composite(), (item.left, item.top))

            img_mask_path = f'data/{product_name}/img_mask/{prefix}{item.name}.png'
            img_mask = Image.open(img_mask_path)

            trimmed = trim_part(img, img_mask.resize(img.size))

            trimmed.save(img_output)


def make_mapping_images(product_name, psd_path):
    print(f'Make model from {psd_path}')

    psd = PSDImage.open(psd_path)

    psd_name = os.path.basename(psd_path).replace('.psd', '')

    for layer in psd:
        if layer.is_group():
            if layer.name == 'Design':
                export_psd_groups(product_name, psd, layer, f'{psd_name}.{layer.name}.')
            else:
                for item in layer:
                    if item.is_group():
                        if item.name == 'Design':
                            export_psd_groups(product_name, psd, item, f'{psd_name}.{layer.name}.{item.name}.')
                        else:
                            for sub_item in item:
                                if sub_item.is_group() and sub_item.name == 'Design':
                                    export_psd_groups(product_name, psd, sub_item
                                                      , f'{psd_name}.{layer.name}.{item.name}.{sub_item.name}.')


def make_img_mapping(product_name):
    psd_template_dir = f'data/{product_name}/psd-artwork'

    for file in glob.glob(f'{psd_template_dir}/*.psd'):
        make_mapping_images(product_name, file)
