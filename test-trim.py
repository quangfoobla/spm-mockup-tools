from PIL import Image
from actions.trim_img_mapping import trim_part

warp = Image.open('data/Hoodie 3D/img_mapping/Front.Front.Design.Right Sleeve.png')
mask = Image.open('data/Hoodie 3D/img_mask/Front.Front.Design.Right Sleeve.png')

trimmed = trim_part(warp, mask)

trimmed.save('test.png')
